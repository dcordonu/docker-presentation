## Qué es Docker

* Docker se ha convertido en el estándar de los contenedores.
* Empresas como Google, IBM, Red Hat o Microsoft trabajan para fomentar el uso de contenedores.
* Docker tiene total dependencia con el kernel de Linux.
* Ligado al mundo _DevOps_.