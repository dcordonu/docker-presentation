## Manejo de contenedores

* Una vez creado, un contenedor puede arrancarse, pararse, borrarse y recrearse.

```bash
docker start debian1
docker attach debian1
docker stop debian1
docker rm debian1
```

* Es posible listar todos los contenedores creados.

```bash
docker ps -a
docker ps -qa | xargs docker rm
```