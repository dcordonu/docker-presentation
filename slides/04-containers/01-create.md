## Crear contenedores

* Los contenedores se crean a partir de imágenes.
* Las imágenes se obtienen desde el repositorio oficial de Docker.

```bash
docker pull debian
```

* A partir de una imagen se pueden crear múltiples contenedores.

```bash
docker create -it --name debian1 debian bash
docker create -it --name debian2 debian bash
```
