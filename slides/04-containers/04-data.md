## Persistencia

* Los contenedores conservan los datos creados en su sistema de archivos hasta que son destruidos.
* Se considera buena práctica hacer que esos ficheros se guarden fuera del contenedor.
* Docker proporciona dos alternativas, **volumes** y **bind mounts**.