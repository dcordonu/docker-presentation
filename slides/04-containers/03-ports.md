## Exponer contenedores

* Un contenedor puede necesitar comunicarse a través de uno o más puertos.
* Mediante el flag `-p` se mapean puertos del host con el contenedor.

```bash
docker run -p 8080:80 -p 9443:443 hermes-apache
```

***

<p style="text-align: left">Acceder en el host a `http://localhost:8080` redirige la petición al puerto 80 del contenedor `hermes-apache`.</p>