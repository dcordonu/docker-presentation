## Persistencia (II). Docker Volumes

* Es la opción recomendada y más potente.
* Independiente de la estructura de ficheros del host.
* Pueden ser gestionados mediante el API de Docker.
* Pueden ser compartidos por varios contenedores.

```bash
docker volume create data
docker volume inspect data
docker run -d -it -v data:/data --name debian1 debian
docker run -d -it -v data:/data --name debian2 debian
docker volume rm data
```