## Persistencia (III). Bind mounts

* Un archivo o sistema de archivos del host es montado en el sistema de archivos del contenedor.
* Bind mounts se recomienda para compartir una estructura de carpetas del host al contenedor, i.e. código fuente, un JAR/WAR/EAR, etc.

<pre><code>docker run -d -it -v $(pwd)/data:/data debian</code></pre>