## Networking (II)

* Docker crea una red de tipo `bridge` donde se conectan todos los contenedores por defecto.
    * Estos contenedores no se ven entre ellos de esta manera, salvo que se les indiquen las IPs expresamente por razones de retrocompatibilidad.
* Para conseguir interconectar contenedores a través de su **hostname** es necesario definir una nueva red.

```bash
docker network create -d bridge blog
```