## Networking

* Por defecto, los contenedores funcionan en una red privada aislada de la red del host.
    * Es posible exponer puertos mediante los flags `-p` y `-P`.
* Al crearse, un contenedor puede configurarse para conectarse de varias maneras.
    * Se indica con el flag `--network`.
    * La configuración por defecto se denomina **bridge**. Otras configuraciones son **host** y **none**, pero no las únicas.