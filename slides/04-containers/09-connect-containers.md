### Networking (III)

* Al crear los contenedores se les indica que se conecten a esa red.

```bash
docker run -d --hostname wordpress --network blog wordpress
```

* Usando una red de tipo `bridge` propia en vez de la de por defecto se consigue que los contenedores puedan conectarse entre sí a través de sus hostnames.

```bash
docker run -d --name wordpress -p 80:80 --network blog \
--hostname wordpress wordpress
docker run -d --name mysql --hostname mysql --network blog \
-e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=wordpress mysql
```