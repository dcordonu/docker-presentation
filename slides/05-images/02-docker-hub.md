## Docker Hub

* Repositorio oficial.
* Permite descargar imágenes oficiales (`library`) o mantenidas por la comunidad.
* Docker Store es la versión empresarial de Docker Hub con contenido de pago.

<hr />

Para obtener la imagen más reciente de Debian:

```bash
docker pull debian
```