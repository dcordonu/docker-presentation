## Uso de Dockerfiles

* Mediante una serie de directivas se configura una imagen basada en otra más genérica.
    * `FROM`
    * `RUN`
    * `EXPOSE`
    * `ADD`
    * `ENTRYPOINT`
    * `CMD`
    * `ENV`