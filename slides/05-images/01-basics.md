## Imágenes

* Los contenedores se construyen a partir **imágenes**.
* Una imagen define:
    * El contenido (archivos) de los contenedores.
    * El comando inicial que es invocado al arrancar el contenedor (por defecto `/bin/sh`).
* Se definen mediante un fichero de configuración (Dockerfile) o manualmente creando un fichero `tar` con el contenido.