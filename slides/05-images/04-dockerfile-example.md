## Uso de Dockerfiles (II)

<pre><code class="Dockerfile"># /data/Dockerfile
FROM library/debian:jessie
RUN apt-get update && apt-get install -y apache2 \
&nbsp;&nbsp;&nbsp;&nbsp;&& ln -sf /dev/stdout /var/log/apache2/access.log \
&nbsp;&nbsp;&nbsp;&nbsp;&& ln -sf /dev/stderr /var/log/apache2/error.log \
&nbsp;&nbsp;&nbsp;&nbsp;&& service apache2 restart
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
EXPOSE 80 443
</code></pre>

* Hemos definido una imagen de Apache2 a partir de una más genérica de Debian Jessie.

```bash
docker build -f /data/Dockerfile -t hermes-apache .
docker run -d --name hermes-apache -p 8080:80 hermes-apache
docker logs -f hermes-apache
```