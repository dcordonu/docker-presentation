## Gestión de Kubernetes - kubectl (I)

* Permite mostrar información del clúster y desplegar contenedores Docker en él.

```bash
kubectl get nodes
kubectl get pods
kubectl run kubernetes-bootcamp \
--image=gcr.io/google-samples/kubernetes-bootcamp:v1 --port=8080
```

* Permite configurar un proxy para comunicarnos con los pods del nodo.

```bash
kubectl proxy
curl http://localhost:8001/version
```