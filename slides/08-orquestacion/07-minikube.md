## Kubernetes en local

* __Minikube__ es una aplicación que permite gestionar una aplicación de Kubernetes que gestiona un único clúster.

```bash
sudo -E minikube start --vm-driver=none
```

* La herramienta que permite gestionar Kubernetes desde la línea de comandos es __kubectl__.

```bash
kubectl version
kubectl cluster-info
```