## Orquestación (III)

* Aprender y dominar una herramienta de orquestación requiere tiempo y esfuerzo.
* Al final, siempre hay una opción que sobresale frente a las demás y se acaba convirtiendo en el estándar _de facto_.
* Es el caso de Kubernetes.