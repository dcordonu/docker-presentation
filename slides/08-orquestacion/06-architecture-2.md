## Arquitectura de Kubernetes (II)

* __Service:__ abstracción para acceder a los pods desplegados en la aplicación.
* __Label:__ Objetos clave-valor que se pueden asociar a objetos de la aplicación.
* __Volume:__ Directorio accesible por todos los contenedores desplegados en un mismo pod.