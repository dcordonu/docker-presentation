## Gestión de Kubernetes - kubectl (III)

* Es posible ejecutar comandos dentro de cada contenedor.

<pre><code class="bash">kubectl exec $POD_NAME env
kubectl exec -ti $POD_NAME bash
</code></pre>

<div style="text-align:left">
    <ul><li>Es posible ver los logs de cada pod.</li></ul>
</div>

<pre><code class="bash">kubectl logs $POD_NAME
</code></pre>