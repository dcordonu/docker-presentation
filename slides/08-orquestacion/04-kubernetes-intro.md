## Kubernetes

* Proporciona mecanismos para:
    * Despliegue de aplicaciones.
    * Actualización.
    * Escalado.
    * Mantenimiento.
    * Scheduling.

* Open source.
* Desarrollado inicialmente por Google pero permite que otras empresas hagan aportaciones al código.