## Orquestación (II)

* Manipular los contenedores para que trabajen en sintonía y que nuestra arquitectura sea capaz de escalar/adaptarse a las necesidades es lo que se denomina __orquestación__.
* Existen varias alternativas para la orquestación de aplicaciones basadas en contenedores Docker:
    * Docker Swarm.
    * CoreOS.
    * Kubernetes.
    * Mesos.