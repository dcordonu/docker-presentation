## Gestión de Kubernetes - kubectl (II)

* Si obtenemos el nombre del pod podemos acceder a él.

<pre><code class="bash">export POD_NAME=$(kubectl get pods -o go-template \
--template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
curl \
localhost:8001/api/v1/proxy/namespaces/default/pods/$POD_NAME
</code></pre>

* Permite controlar los contenedores que hay desplegados en cada pod.

```bash
kubectl describe pods
```