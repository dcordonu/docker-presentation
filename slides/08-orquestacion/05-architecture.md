## Arquitectura de Kubernetes (I)

* __Pod:__ unidad de despliegue de Kubernetes, formada por uno o varios contenedores Docker.
* __Minion:__ _worker node_ donde se despliegan pods. Contiene los servicios necesarios para interactuar con ellos: docker, kubelet, proxy.
* __Master:__ uno o varios hosts con servicios para controlar los clústeres: API para que master y el resto de nodos interactúen, scheduler y control de réplicas.