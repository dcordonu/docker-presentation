## Gestión de Kubernetes - kubectl (V)

* Kubernetes gestiona el escalado de la aplicación.
    * Creando más pods.
    * Configurando un balanceador de carga para enviar el tráfico a todos los pods disponibles en cada momento.

<pre><code class="bash">kubectl scale deployments/kubernetes-bootcamp --replicas=4
kubectl get deployments
kubectl get pods -o wide
curl $(minikube ip):$NODE_PORT
</code></pre>