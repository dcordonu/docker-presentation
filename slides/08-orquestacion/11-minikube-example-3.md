## Gestión de Kubernetes - kubectl (IV)

* Para poder acceder a los contenedores es necesario crear un service que agrupe uno o varios pods.

<pre><code class="bash">kubectl get services
kubectl expose deployment/kubernetes-bootcamp \
--type="NodePort" --port 8080
</code></pre>

* Obtenemos el puerto expuesto para comprobar que el servicio está levantado:

<pre><code class="bash">export NODE_PORT=$(kubectl get services/kubernetes-bootcamp \
-o go-template='{{(index .spec.ports 0).nodePort}}')
curl $(minikube ip):$NODE_PORT
</code></pre>
