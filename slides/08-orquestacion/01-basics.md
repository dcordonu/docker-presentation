## Orquestación

* En entornos de desarrollo, arrancar una instancia de cada contenedor es suficiente.
* En entornos de producción, las necesidades son más complejas:
    * Poder levantar varias instancias y en más de un servidor.
    * Monitorizar el ciclo de vida de esos contenedores.