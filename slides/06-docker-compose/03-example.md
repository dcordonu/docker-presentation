## Docker-Compose - Ejemplo

<pre><code class="yaml">version: '3.1'
services:
    mysql:
        image: mysql
        container_name: mysql
        restart: always
        environment:
            MYSQL_ROOT_PASSWORD: password
            MYSQL_DATABASE: wordpress
    wordpress:
        container_name: wordpress
        image: wordpress
        restart: always
        ports:
            - "80:80"
</code></pre>