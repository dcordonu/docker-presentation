## Docker-compose

* Lanzar uno o dos contenedores con `docker run` que se comunican entre sí es sencillo.
* Si la aplicación está compuesta por más contenedores se requieren más comandos para arrancar el entorno.
* La herramienta **Docker-compose** permite reunir todos los parámetros de configuración de los contenedores en un único fichero **YAML**.