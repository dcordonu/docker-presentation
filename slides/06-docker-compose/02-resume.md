## Docker-Compose (II)

* El comando `docker-compose -f blog.yml up -d` crea y lanza los contenedores en background.
* Los contenedores así creados pueden borrarse o inspeccionarse como cualquier otro.

```bash
docker inspect wordpress
docker stop wordpress mysql && docker rm wordpress mysql
```